{.passC:"-Isubmodules/console/include".}
when defined windows:
  {.compile:"../submodules/console/src/windows_shim.c".}
else:
  {.compile:"../submodules/console/src/unix_shim.c".}


func fg_set_black*(bold: uint8) {.cdecl, importc.}
func fg_set_red*(bold: uint8) {.cdecl, importc.}
func fg_set_blue*(bold: uint8) {.cdecl, importc.}
func fg_set_cyan*(bold: uint8) {.cdecl, importc.}
func fg_set_green*(bold: uint8) {.cdecl, importc.}
func fg_set_yellow*(bold: uint8) {.cdecl, importc.}
func fg_set_magenta*(bold: uint8) {.cdecl, importc.}
func fg_set_white*(bold: uint8) {.cdecl, importc.}

func bg_set_black*(bold: uint8) {.cdecl, importc.}
func bg_set_red*(bold: uint8) {.cdecl, importc.}
func bg_set_blue*(bold: uint8) {.cdecl, importc.}
func bg_set_cyan*(bold: uint8) {.cdecl, importc.}
func bg_set_green*(bold: uint8) {.cdecl, importc.}
func bg_set_yellow*(bold: uint8) {.cdecl, importc.}
func bg_set_magenta*(bold: uint8) {.cdecl, importc.}
func bg_set_white*(bold: uint8) {.cdecl, importc.}

func termreset*() {.cdecl, importc:"reset".}

#Nimified variants
func fg_set_black*(bold: bool = false) =
  if bold: fg_set_black(1)
  else: fg_set_black(0)

func fg_set_red*(bold: bool = false) =
  if bold: fg_set_red(1)
  else: fg_set_red(0)

func fg_set_blue*(bold: bool = false) =
  if bold: fg_set_red(1)
  else: fg_set_red(0)

func fg_set_cyan*(bold: bool = false) =
  if bold: fg_set_cyan(1)
  else: fg_set_cyan(0)

func fg_set_green*(bold: bool = false) =
  if bold: fg_set_green(1)
  else: fg_set_green(0)

func fg_set_yellow*(bold: bool = false) =
  if bold: fg_set_yellow(1)
  else: fg_set_yellow(0)

func fg_set_magenta*(bold: bool = false) =
  if bold: fg_set_magenta(1)
  else: fg_set_magenta(0)

func fg_set_white*(bold: bool = false) =
  if bold: fg_set_white(1)
  else: fg_set_white(0)


func bg_set_black*(bold: bool = false) =
  if bold: bg_set_black(1)
  else: bg_set_black(0)

func bg_set_red*(bold: bool = false) =
  if bold: bg_set_red(1)
  else: bg_set_red(0)

func bg_set_blue*(bold: bool = false) =
  if bold: bg_set_blue(1)
  else: bg_set_blue(0)

func bg_set_cyan*(bold: bool = false) =
  if bold: bg_set_cyan(1)
  else: bg_set_cyan(0)

func bg_set_green*(bold: bool = false) =
  if bold: bg_set_green(1)
  else: bg_set_green(0)

func bg_set_yellow*(bold: bool = false) =
  if bold: bg_set_yellow(1)
  else: bg_set_yellow(0)

func bg_set_magenta*(bold: bool = false) =
  if bold: bg_set_magenta(1)
  else: bg_set_magenta(0)

func bg_set_white*(bold: bool = false) =
  if bold: bg_set_white(1)
  else: bg_set_white(0)

