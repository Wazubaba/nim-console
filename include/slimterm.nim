import console

const
  FGBLACK*   = "\f[0"
  FGRED*     = "\f[1"
  FGBLUE*    = "\f[2"
  FGCYAN*    = "\f[3"
  FGGREEN*   = "\f[4"
  FGYELLOW*  = "\f[5"
  FGMAGENTA* = "\f[6"
  FGWHITE*   = "\f[7"

  FGBBLACK*   = "\f[8"
  FGBRED*     = "\f[9"
  FGBBLUE*    = "\f[a"
  FGBCYAN*    = "\f[b"
  FGBGREEN*   = "\f[c"
  FGBYELLOW*  = "\f[d"
  FGBMAGENTA* = "\f[e"
  FGBWHITE*   = "\f[f"

  BGBLACK*   = "\f[g"
  BGRED*     = "\f[h"
  BGBLUE*    = "\f[i"
  BGCYAN*    = "\f[j"
  BGGREEN*   = "\f[k"
  BGYELLOW*  = "\f[l"
  BGMAGENTA* = "\f[m"
  BGWHITE*   = "\f[n"

  BGBBLACK*   = "\f[o"
  BGBRED*     = "\f[p"
  BGBBLUE*    = "\f[q"
  BGBCYAN*    = "\f[r"
  BGBGREEN*   = "\f[s"
  BGBYELLOW*  = "\f[t"
  BGBMAGENTA* = "\f[u"
  BGBWHITE*   = "\f[v"

  RESET* = "\f[z"

proc colorize*(content: varargs[string, `$`]) =
  #[[
    Colorize works a bit like the stdlib's terminal `styled*` functions
    do, but instead of being a template or macro just takes printf-style
    args that resolve to color commands. The reason for this is because
    I don't understand the macro system and tbqh I feel it creates too
    much bloat to be used for something intended to be simple and
    low-level.

    Each color is exposed as a const variable that will be named after
    the given color code, which will internally use a system similar to
    ansi escapes but with a twist.
  ]]#

  for item in content:
    case item:
      of FGBLACK: fg_set_black()
      of FGRED: fg_set_red()
      of FGBLUE: fg_set_blue()
      of FGCYAN: fg_set_cyan()
      of FGGREEN: fg_set_green()
      of FGYELLOW: fg_set_yellow()
      of FGMAGENTA: fg_set_magenta()
      of FGWHITE: fg_set_white()

      of FGBBLACK: fg_set_black(true)
      of FGBRED: fg_set_red(true)
      of FGBBLUE: fg_set_blue(true)
      of FGBCYAN: fg_set_cyan(true)
      of FGBGREEN: fg_set_green(true)
      of FGBYELLOW: fg_set_yellow(true)
      of FGBMAGENTA: fg_set_magenta(true)
      of FGBWHITE: fg_set_white(true)

      of BGBLACK: bg_set_black()
      of BGRED: bg_set_red()
      of BGBLUE: bg_set_blue()
      of BGCYAN: bg_set_cyan()
      of BGGREEN: bg_set_green()
      of BGYELLOW: bg_set_yellow()
      of BGMAGENTA: bg_set_magenta()
      of BGWHITE: bg_set_white()

      of BGBBLACK: bg_set_black(true)
      of BGBRED: bg_set_red(true)
      of BGBBLUE: bg_set_blue(true)
      of BGBCYAN: bg_set_cyan(true)
      of BGBGREEN: bg_set_green(true)
      of BGBYELLOW: bg_set_yellow(true)
      of BGBMAGENTA: bg_set_magenta(true)
      of BGBWHITE: bg_set_white(true)

      of RESET: termreset()

      else:
        write(stdout, item)
  echo("")
  termreset()

