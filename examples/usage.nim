import console
import slimterm

fg_set_green()
echo "Here is green text"
bg_set_blue(true)
echo "Here is green text on a blue background which is bold"
termreset()
echo "And here are all attributes restored to normal"


echo "And here's a test of the slimterm functionality"

colorize("This ", FGBBLUE, "green ", RESET, "text can also be ", FGBBLUE, BGRED, "bright blue on a red background")
echo "Note that there is no need to reset the term afterwards as colorize will always do so"

